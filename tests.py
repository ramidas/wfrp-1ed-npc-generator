#! /usr/bin/python
# -*- coding: utf-8 -*-

import unittest
from classNpc import *

class testy(unittest.TestCase):

	def setUp(self):
		self.npc = classNpc()
		assert isinstance(self.npc, classNpc)

	def test_kostki_dzialaja(self):
		for n in xrange(100):		# liczba testów
			for i in xrange(1,20):
				for kostka in [2,3,4,6,8,10,12,20]:
					assert i <= k(i,kostka) <= i*kostka

	def test_czlowiek_ma_poprawne_statystyki(self):
		npc = [ classNpc(rasa='czlowiek') for i in xrange(100) ]
		for i in npc:
			assert 3 <= i.stats['Sz'] <= 5
			assert 22 <= i.stats['WW'] <= 40
			assert 22 <= i.stats['US'] <= 40
			assert 2 <= i.stats['S'] <= 4
			assert 2 <= i.stats['Wt'] <= 4
			assert 5 <= i.stats['Zw'] <= 7
			assert 22 <= i.stats['I'] <= 40
			assert i.stats['A'] == 1
			assert 22 <= i.stats['Zr'] <= 40 
			assert 22 <= i.stats['CP'] <= 40
			assert 22 <= i.stats['Int'] <= 40
			assert 22 <= i.stats['Op'] <= 40
			assert 22 <= i.stats['SW'] <= 40
			assert 22 <= i.stats['Ogd'] <= 40

	def test_elf_ma_poprawne_statystyki(self):
		npc = [ classNpc(rasa='elf') for i in xrange(100) ]
		for i in npc:
			assert 3 <= i.stats['Sz'] <= 5
			assert 32 <= i.stats['WW'] <= 50
			assert 22 <= i.stats['US'] <= 40
			assert 2 <= i.stats['S'] <= 4
			assert 2 <= i.stats['Wt'] <= 4
			assert 4 <= i.stats['Zw'] <= 6
			assert 52 <= i.stats['I'] <= 70
			assert i.stats['A'] == 1
			assert 32 <= i.stats['Zr'] <= 50
			assert 32 <= i.stats['CP'] <= 50
			assert 42 <= i.stats['Int'] <= 60
			assert 42 <= i.stats['Op'] <= 60
			assert 32 <= i.stats['SW'] <= 50
			assert 32 <= i.stats['Ogd'] <= 50

	def test_krasnolud_ma_poprawne_statystyki(self):
		npc = [ classNpc(rasa='krasnolud') for i in xrange(100) ]
		for i in npc:
			assert 3 <= i.stats['Sz'] <= 4
			assert 32 <= i.stats['WW'] <= 50
			assert 12 <= i.stats['US'] <= 30
			assert 2 <= i.stats['S'] <= 4
			assert 3 <= i.stats['Wt'] <= 5
			assert 6 <= i.stats['Zw'] <= 8
			assert 12 <= i.stats['I'] <= 30
			assert i.stats['A'] == 1
			assert 12 <= i.stats['Zr'] <= 30
			assert 42 <= i.stats['CP'] <= 60
			assert 22 <= i.stats['Int'] <= 40
			assert 42 <= i.stats['Op'] <= 60
			assert 42 <= i.stats['SW'] <= 60
			assert 12 <= i.stats['Ogd'] <= 30
			
	def test_halfling_ma_poprawne_statystyki(self):
		npc = [ classNpc(rasa='halfling') for i in xrange(100) ]
		for i in npc:
			assert 3 <= i.stats['Sz'] <= 4
			assert 12 <= i.stats['WW'] <= 30
			assert 22 <= i.stats['US'] <= 40
			assert 1 <= i.stats['S'] <= 3
			assert 1 <= i.stats['Wt'] <= 3
			assert 4 <= i.stats['Zw'] <= 6
			assert 42 <= i.stats['I'] <= 60
			assert i.stats['A'] == 1
			assert 32 <= i.stats['Zr'] <= 50
			assert 12 <= i.stats['CP'] <= 30
			assert 22 <= i.stats['Int'] <= 40
			assert 12 <= i.stats['Op'] <= 30
			assert 32 <= i.stats['SW'] <= 50
			assert 32 <= i.stats['Ogd'] <= 50

	def test_npc_ma_klase(self):
		npc = [ classNpc() for i in xrange(1000) ]
		for i in npc:
			assert i.klasa in ['wojownik', 'ranger', 'lotr', 'uczony' ]
			if i.klasa == 'wojownik': assert i.stats['WW'] >= 30
			if i.klasa == 'ranger': assert i.stats['US'] >= 30
			if i.klasa == 'lotr': assert (i.rasa != 'elf' and i.stats['I'] >= 30) or (i.rasa == 'elf' and i.stats['I']) >= 65
			if i.klasa == 'uczony': assert i.stats['Int'] >= 30 and i.stats['SW'] >= 30
		

if __name__ == "__main__":
	unittest.main()
