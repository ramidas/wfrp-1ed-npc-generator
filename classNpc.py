#! /usr/bin/python
# -*- coding: utf-8 -*-

from random import choice, randint

rasy = [ u"czlowiek", u"elf", u"krasnolud", u"halfling" ]

def k(n,k):
	suma = 0
	for i in xrange(n):
		suma += randint(1,k)
	return suma

def getPP(rasa):
	if rasa == u"czlowiek": return k(1,3)+1
	elif rasa == u"elf": return max(k(1,3)-1,1)
	elif rasa == u"krasnolud": return k(1,3)
	elif rasa == u"halfling": return k(1,4)

def getWiek(rasa):
	typ = choice([u"młodszy",u"starszy"])
	wiek = 0
	if typ == u"młodszy":
		if rasa == u"czlowiek":
			wiek = k(6,6)
			while wiek < 16:
				wiek += k(1,6)		 
			return wiek
		elif rasa == u"elf":
			wiek = k(10,12)
			while wiek < 16:
				wiek += k(1,12)		 
			return wiek
		elif rasa == u"krasnolud": 
			wiek = k(9,12)
			while wiek < 16:
				wiek += k(1,12)		 
			return wiek
		elif rasa == u"halfling":  
			wiek = k(10,6)
			while wiek < 16:
				wiek += k(1,6)		 
			return wiek
	if typ == u"starszy":
		if rasa == u"czlowiek":
			wiek = k(6,10)
			while wiek < 16:
				wiek += k(1,10)		 
			return wiek
		elif rasa == u"elf":
			wiek = k(10,20)
			while wiek < 16:
				wiek += k(1,20)		 
			return wiek
		elif rasa == u"krasnolud": 
			wiek = k(9,20)
			while wiek < 16:
				wiek += k(1,20)		 
			return wiek
		elif rasa == u"halfling":  
			wiek = k(10,12)
			while wiek < 16:
				wiek += k(1,12)		 
			return wiek

def getBasicStats(rasa):
	stats = {}
	if rasa == u'czlowiek':
		stats['Sz'] = k(1,3) + 2
		stats['WW'] = k(2,10) + 20 
		stats['US'] = k(2,10) + 20
		stats['S'] = k(1,3) + 1 
		stats['Wt'] = k(1,3) + 1 
		stats['Zw'] = k(1,3) + 4 
		stats['I'] = k(2,10) + 20
		stats['A'] = 1
		stats['Zr'] = k(2,10) + 20 
		stats['CP'] = k(2,10) + 20
		stats['Int'] = k(2,10) + 20
		stats['Op'] = k(2,10) + 20
		stats['SW'] = k(2,10) + 20
		stats['Ogd'] = k(2,10) + 20
	elif rasa == u'elf':
		stats['Sz'] = k(1,3) + 2
		stats['WW'] = k(2,10) + 30 
		stats['US'] = k(2,10) + 20
		stats['S'] = k(1,3) + 1 
		stats['Wt'] = k(1,3) + 1 
		stats['Zw'] = k(1,3) + 3
		stats['I'] = k(2,10) + 50
		stats['A'] = 1
		stats['Zr'] = k(2,10) + 30 
		stats['CP'] = k(2,10) + 30
		stats['Int'] = k(2,10) + 40
		stats['Op'] = k(2,10) + 40
		stats['SW'] = k(2,10) + 30
		stats['Ogd'] = k(2,10) + 30
	elif rasa == u'krasnolud':
		stats['Sz'] = k(1,2) + 2
		stats['WW'] = k(2,10) + 30 
		stats['US'] = k(2,10) + 10
		stats['S'] = k(1,3) + 1
		stats['Wt'] = k(1,3) + 2 
		stats['Zw'] = k(1,3) + 5 
		stats['I'] = k(2,10) + 10
		stats['A'] = 1
		stats['Zr'] = k(2,10) + 10 
		stats['CP'] = k(2,10) + 40
		stats['Int'] = k(2,10) + 20
		stats['Op'] = k(2,10) + 40
		stats['SW'] = k(2,10) + 40
		stats['Ogd'] = k(2,10) + 10
	elif rasa == u'halfling':
		stats['Sz'] = k(1,2) + 2
		stats['WW'] = k(2,10) + 10 
		stats['US'] = k(2,10) + 20
		stats['S'] = k(1,3)
		stats['Wt'] = k(1,3) 
		stats['Zw'] = k(1,3) + 3
		stats['I'] = k(2,10) + 40
		stats['A'] = 1
		stats['Zr'] = k(2,10) + 30 
		stats['CP'] = k(2,10) + 10
		stats['Int'] = k(2,10) + 20
		stats['Op'] = k(2,10) + 10
		stats['SW'] = k(2,10) + 30
		stats['Ogd'] = k(2,10) + 30
	return stats
	
def getKlasa(stats,rasa):
	mozliweKlasy = []
	if stats['WW'] >= 30: mozliweKlasy.append('wojownik')
	if stats['US'] >= 30: mozliweKlasy.append('ranger')
	if (rasa != 'elf' and stats['I'] >= 30) or (rasa == 'elf' and stats['I'] >= 65) : mozliweKlasy.append('lotr')
	if stats['Int'] >= 30 and stats['SW'] >= 30: mozliweKlasy.append('uczony')
	if len(mozliweKlasy) == 0: return "pechowiec"	# Klasa postaci, o statystkach za slabych na cokolwiek
	return choice(mozliweKlasy)

def getUmiejetnoscZTabeli(rasa, klasa):
	los = k(1,100)
	if klasa == 'wojownik':
		if rasa == 'czlowiek':
			if 1 <= los <= 5 : return 'Czuly sluch'
			if 6 <= los <= 10: return 'Oburecznosc'
			if 11 <= los <= 15: return 'Taniec' 
			if 16 <= los <= 20: return 'Rozbrajanie'
			if 21 <= los <= 25: return 'Uniki'
			if 26 <= los <= 30: return 'Powozenie'
			if 31 <= los <= 35: return 'Bystry wzrok'
			if 36 <= los <= 40: return 'Bardzo szybki'
			if 41 <= los <= 45: return 'Szybki refleks'
			if 46 <= los <= 50: return 'Szczescie'
			if 51 <= los <= 55: return 'Widzenie w ciemnosciach'
			if 56 <= los <= 65: return 'Czytanie/Pisanie'
			if 66 <= los <= 75: return 'Jezdziectwo'
			if 76 <= los <= 80: return 'Wspinaczka'
			if 81 <= los <= 85: return 'Spiew'
			if 86 <= los <= 90: return 'Szosty zmysl'
			if 91 <= los <= 95: return 'Bardzo wytrzymaly'
			if 96 <= los <= 100: return 'Bardzo silny'
		if rasa == 'elf':
			if 1 <= los <= 5 : return 'Czuly sluch'
			if 6 <= los <= 10: return 'Oburecznosc'
			if 11 <= los <= 15: return 'Taniec' 
			if 16 <= los <= 20: return 'Rozbrajanie'
			if 21 <= los <= 25: return 'Uniki'
			if 26 <= los <= 30: return 'Powozenie'
			if 31 <= los <= 35: return 'Bystry wzrok'
			if 36 <= los <= 40: return 'Bardzo szybki'
			if 41 <= los <= 45: return 'Szybki refleks'
			if 46 <= los <= 50: return 'Szczescie'
			if 51 <= los <= 55: return 'Widzenie w ciemnosciach'
			if 56 <= los <= 65: return 'Czytanie/Pisanie'
			if 66 <= los <= 70: return 'Jezdziectwo'
			if 71 <= los <= 75: return 'Wspinaczka'
			if 76 <= los <= 80: return 'Cichy chod na wsi'
			if 81 <= los <= 90: return 'Spiew'
			if 91 <= los <= 95: return 'Szosty zmysl'
			if 96 <= los <= 100: return 'Bardzo wytrzymaly'
		if rasa == 'krasnolod':
			if 1 <= los <= 5 : return 'Czuly sluch'
			if 6 <= los <= 10: return 'Oburecznosc'
			if 11 <= los <= 15: return 'Taniec' 
			if 16 <= los <= 20: return 'Rozbrajanie'
			if 21 <= los <= 25: return 'Uniki'
			if 26 <= los <= 30: return 'Powozenie'
			if 31 <= los <= 35: return 'Bystry wzrok'
			if 36 <= los <= 40: return 'Bardzo szybki'
			if 41 <= los <= 45: return 'Szybki refleks'
			if 46 <= los <= 50: return 'Widzenie w ciemnosciach'
			if 51 <= los <= 60: return 'Czytanie/Pisanie'
			if 61 <= los <= 65: return 'Wspinaczka'
			if 66 <= los <= 70: return 'Spiew'
			if 71 <= los <= 75: return 'Szosty zmysl'
			if 76 <= los <= 90: return 'Bardzo wytrzymaly'
			if 91 <= los <= 100: return 'Bardzo wytrzymaly'
		if rasa == 'halfling':
			if 1 <= los <= 5 : return 'Czuly sluch'
			if 6 <= los <= 10: return 'Oburecznosc'
			if 11 <= los <= 15: return 'Taniec' 
			if 16 <= los <= 20: return 'Rozbrajanie'
			if 21 <= los <= 25: return 'Uniki'
			if 26 <= los <= 30: return 'Powozenie'
			if 31 <= los <= 35: return 'Bystry wzrok'
			if 36 <= los <= 40: return 'Bardzo szybki'
			if 41 <= los <= 45: return 'Szybki refleks'
			if 46 <= los <= 50: return 'Szczescie'
			if 51 <= los <= 55: return 'Widzenie w ciemnosciach'
			if 56 <= los <= 65: return 'Czytanie/Pisanie'
			if 66 <= los <= 70: return 'Wspinaczka'
			if 71 <= los <= 75: return 'Cichy chod na wsi'
			if 76 <= los <= 80: return 'Cichy chod w miescie'
			if 81 <= los <= 85: return 'Spiew'
			if 86 <= los <= 90: return 'Szosty zmysl'
			if 91 <= los <= 95: return 'Bardzo wytrzymaly'
			if 96 <= los <= 100: return 'Bardzo silny'
	if klasa == 'ranger':
		if rasa == 'czlowiek':
			if 1 <= los <= 5: return 'Czuly sluch'
			if 6 <= los <= 10: return 'Oburecznosc'
			if 11 <= los <= 15: return 'Astronomia'
			if 16 <= los <= 20: return 'Taniec' 
			if 21 <= los <= 30: return 'Powozenie'
			if 31 <= los <= 35: return 'Bystry wzrok'
			if 36 <= los <= 40: return 'Bardzo szybki'
			if 41 <= los <= 45: return 'Szybki refleks'
			if 46 <= los <= 50: return 'Szczescie'
			if 51 <= los <= 55: return 'Widzenie w ciemnosciach'
			if 56 <= los <= 60: return 'Wyczucie kierunku'
			if 61 <= los <= 65: return 'Warzenie trucizn (ziolowych)'
			if 66 <= los <= 70: return 'Czytanie/Pisanie'
			if 71 <= los <= 75: return 'Jezdziectwo'
			if 76 <= los <= 80: return 'Wspinaczka'
			if 81 <= los <= 85: return 'Spiew'
			if 86 <= los <= 90: return 'Szosty zmysl'
			if 91 <= los <= 95: return 'Bardzo wytrzymaly'
			if 96 <= los <= 100: return 'Bardzo silny'
		if rasa == 'elf':
			if 1 <= los <= 5: return 'Czuly sluch'
			if 6 <= los <= 10: return 'Oburecznosc'
			if 11 <= los <= 15: return 'Astronomia'
			if 16 <= los <= 20: return 'Taniec' 
			if 21 <= los <= 25: return 'Powozenie'
			if 26 <= los <= 35: return 'Bardzo szybki'
			if 36 <= los <= 40: return 'Szybki refleks'
			if 41 <= los <= 45: return 'Szczescie'
			if 46 <= los <= 50: return 'Widzenie w ciemnosciach'
			if 51 <= los <= 55: return 'Wyczucie kierunku'
			if 56 <= los <= 60: return 'Warzenie trucizn (ziolowych)'
			if 61 <= los <= 65: return 'Czytanie/Pisanie'
			if 66 <= los <= 70: return 'Jezdziectwo'
			if 71 <= los <= 75: return 'Wspinaczka'
			if 76 <= los <= 80: return 'Cichy chod na wsi'
			if 81 <= los <= 90: return 'Spiew'
			if 91 <= los <= 95: return 'Szosty zmysl'
			if 96 <= los <= 100: return 'Bardzo wytrzymaly'
		if rasa == 'krasnolud':
			if 1 <= los <= 5: return 'Czuly sluch'
			if 6 <= los <= 10: return 'Oburecznosc'
			if 11 <= los <= 15: return 'Astronomia'
			if 16 <= los <= 20: return 'Taniec' 
			if 21 <= los <= 30: return 'Powozenie'
			if 31 <= los <= 35: return 'Bystry wzrok'
			if 36 <= los <= 40: return 'Bardzo szybki'
			if 41 <= los <= 45: return 'Szybki refleks'
			if 46 <= los <= 50: return 'Widzenie w ciemnosciach'
			if 51 <= los <= 55: return 'Wyczucie kierunku'
			if 56 <= los <= 60: return 'Czytanie/Pisanie'
			if 61 <= los <= 61: return 'Wspinaczka'
			if 66 <= los <= 70: return 'Spiew'
			if 71 <= los <= 75: return 'Szosty zmysl'
			if 76 <= los <= 90: return 'Bardzo wytrzymaly'
			if 91 <= los <= 100: return 'Bardzo silny'
		if rasa == 'halfling':
			if 1 <= los <= 5: return 'Czuly sluch'
			if 6 <= los <= 10: return 'Oburecznosc'
			if 11 <= los <= 15: return 'Astronomia'
			if 16 <= los <= 20: return 'Taniec' 
			if 21 <= los <= 30: return 'Powozenie'
			if 31 <= los <= 35: return 'Bystry wzrok'
			if 36 <= los <= 40: return 'Bardzo szybki'
			if 41 <= los <= 45: return 'Szybki refleks'
			if 46 <= los <= 50: return 'Szczescie'
			if 51 <= los <= 55: return 'Widzenie w ciemnosciach'
			if 56 <= los <= 60: return 'Wyczucie kierunku'
			if 61 <= los <= 65: return 'Warzenie trucizn (ziolowych)'
			if 66 <= los <= 70: return 'Czytanie/Pisanie'
			if 71 <= los <= 75: return 'Wspinaczka'
			if 76 <= los <= 80: return 'Cichy chod na wsi'
			if 81 <= los <= 85: return 'Spiew'
			if 86 <= los <= 90: return 'Szosty zmysl'
			if 91 <= los <= 95: return 'Bardzo wytrzymaly'
			if 96 <= los <= 100: return 'Bardzo silny'
	if klasa == 'lotr':
		if rasa == 'czlowiek':
			if 1 <= los <= 5: return 'Czuly sluch'
			if 6 <= los <= 10: return 'Oburecznosc'
			if 11 <= los <= 15: return 'Gadanina'
			if 16 <= los <= 20: return 'Przekupstwo'
			if 21 <= los <= 25: return 'Taniec' 
			if 26 <= los <= 30: return 'Uniki'
			if 31 <= los <= 35: return 'Bystry wzrok'
			if 36 <= los <= 40: return 'Ucieczka'
			if 41 <= los <= 45: return 'Bardzo szybki'
			if 46 <= los <= 50: return 'Szybki refleks'
			if 51 <= los <= 55: return 'Szczescie'
			if 56 <= los <= 60: return 'Widzenie w ciemnosciach'
			if 61 <= los <= 65: return 'Jezdziectwo'
			if 66 <= los <= 70: return 'Wspinaczka'
			if 71 <= los <= 75: return 'Cichy chod na wsi'
			if 76 <= los <= 80: return 'Cichy chod w miescie'
			if 81 <= los <= 85: return 'Spiew'
			if 86 <= los <= 90: return 'Szosty zmysl'
			if 91 <= los <= 95: return 'Bijatyka'
			if 96 <= los <= 100: return 'Bardzo wytrzymaly'
		if rasa == 'elf':
			if 1 <= los <= 5: return 'Czuly sluch'
			if 6 <= los <= 10: return 'Oburecznosc'
			if 11 <= los <= 15: return 'Gadanina'
			if 16 <= los <= 20: return 'Przekupstwo'
			if 21 <= los <= 25: return 'Taniec' 
			if 26 <= los <= 30: return 'Uniki'
			if 31 <= los <= 35: return 'Bystry wzrok'
			if 36 <= los <= 40: return 'Ucieczka'
			if 41 <= los <= 45: return 'Bardzo szybki'
			if 46 <= los <= 50: return 'Szybki refleks'
			if 51 <= los <= 55: return 'Szczescie'
			if 56 <= los <= 60: return 'Widzenie w ciemnosciach'
			if 61 <= los <= 65: return 'Jezdziectwo'
			if 66 <= los <= 70: return 'Wspinaczka'
			if 71 <= los <= 75: return 'Cichy chod na wsi'
			if 76 <= los <= 80: return 'Cichy chod w miescie'
			if 81 <= los <= 85: return 'Spiew'
			if 86 <= los <= 90: return 'Szosty zmysl'
			if 91 <= los <= 95: return 'Bijatyka'
			if 96 <= los <= 100: return 'Bardzo silny'
		if rasa == 'krasnolud':
			if 1 <= los <= 5: return 'Czuly sluch'
			if 6 <= los <= 10: return 'Oburecznosc'
			if 11 <= los <= 15: return 'Gadanina'
			if 16 <= los <= 20: return 'Przekupstwo'
			if 21 <= los <= 25: return 'Taniec' 
			if 26 <= los <= 30: return 'Uniki'
			if 31 <= los <= 35: return 'Bystry wzrok'
			if 36 <= los <= 40: return 'Ucieczka'
			if 41 <= los <= 45: return 'Szczescie'
			if 46 <= los <= 50: return 'Widzenie w ciemnosciach'
			if 51 <= los <= 55: return 'Wspinaczka'
			if 56 <= los <= 60: return 'Spiew'
			if 61 <= los <= 65: return 'Szosty zmysl'
			if 66 <= los <= 70: return 'Bijatyka'
			if 71 <= los <= 90: return 'Bardzo wytrzymaly'
			if 91 <= los <= 100: return 'Bardzo silny'
		if rasa == 'halfling':
			if 1 <= los <= 5: return 'Czuly sluch'
			if 6 <= los <= 10: return 'Oburecznosc'
			if 11 <= los <= 15: return 'Gadanina'
			if 16 <= los <= 20: return 'Przekupstwo'
			if 21 <= los <= 25: return 'Taniec' 
			if 26 <= los <= 30: return 'Uniki'
			if 31 <= los <= 35: return 'Bystry wzrok'
			if 36 <= los <= 40: return 'Ucieczka'
			if 41 <= los <= 45: return 'Szybki refleks'
			if 46 <= los <= 50: return 'Szczescie'
			if 51 <= los <= 55: return 'Widzenie w ciemnosciach'
			if 56 <= los <= 60: return 'Wspinaczka'
			if 61 <= los <= 65: return 'Cichy chod na wsi'
			if 66 <= los <= 70: return 'Cichy chod w miescie'
			if 71 <= los <= 75: return 'Spiew'
			if 76 <= los <= 80: return 'Szosty zmysl'
			if 81 <= los <= 85: return 'Bijatyka'
			if 86 <= los <= 95: return 'Bardzo wytrzymaly'
			if 96 <= los <= 100: return 'Bardzo silny'
	if klasa == 'uczony':
		if rasa == 'czlowiek':
			if 1 <= los <= 5: return 'Czuly sluch'
			if 6 <= los <= 10: return 'Oburecznosc'
			if 11 <= los <= 15: return 'Astronomia'
			if 16 <= los <= 20: return 'Gadanina'
			if 21 <= los <= 25: return 'Kryptografia'
			if 26 <= los <= 30: return 'Taniec'
			if 31 <= los <= 35: return 'Powozenie'
			if 36 <= los <= 40: return 'Etykieta'
			if 41 <= los <= 45: return 'Bystry wzrok'
			if 46 <= los <= 50: return 'Ucieczka'
			if 51 <= los <= 55: return 'Heraldyka'
			if 56 <= los <= 60: return 'Szybki refleks'
			if 61 <= los <= 65: return 'Szczescie'
			if 66 <= los <= 70: return 'Czytanie/Pisanie'
			if 71 <= los <= 75: return 'Jezdziectwo'
			if 76 <= los <= 80: return 'Cichy chod w miescie'
			if 81 <= los <= 85: return 'Szosty zmysl'
			if 86 <= los <= 90: return 'Geniusz arytmetyczny'
			if 91 <= los <= 95: return 'Bardzo wytrzymaly'
			if 96 <= los <= 100: return 'Bardzo silny'
		if rasa == 'elf':
			if 1 <= los <= 5: return 'Czuly sluch'
			if 6 <= los <= 10: return 'Oburecznosc'
			if 11 <= los <= 15: return 'Astronomia'
			if 16 <= los <= 20: return 'Gadanina'
			if 21 <= los <= 25: return 'Kryptografia'
			if 26 <= los <= 30: return 'Taniec'
			if 31 <= los <= 35: return 'Etykieta'
			if 36 <= los <= 40: return 'Bystry wzrok'
			if 41 <= los <= 45: return 'Ucieczka'
			if 46 <= los <= 50: return 'Bardzo Szybki'
			if 51 <= los <= 55: return 'Heraldyka'
			if 56 <= los <= 60: return 'Szybki refleks'
			if 61 <= los <= 65: return 'Szczescie'
			if 66 <= los <= 70: return 'Czytanie/Pisanie'
			if 71 <= los <= 75: return 'Jezdziectwo'
			if 76 <= los <= 80: return 'Cichy chod na wsi'
			if 81 <= los <= 85: return 'Spiew'
			if 86 <= los <= 90: return 'Szosty zmysl'
			if 91 <= los <= 95: return 'Geniusz arytmetyczny'
			if 96 <= los <= 100: return 'Bardzo wytrzymaly'
		if rasa == 'krasnolud':
			if 1 <= los <= 5: return 'Czuly sluch'
			if 6 <= los <= 10: return 'Oburecznosc'
			if 11 <= los <= 15: return 'Gadanina'
			if 16 <= los <= 20: return 'Kryptografia'
			if 21 <= los <= 25: return 'Taniec'
			if 26 <= los <= 30: return 'Powozenie'
			if 31 <= los <= 35: return 'Bystry wzrok'
			if 36 <= los <= 40: return 'Ucieczka'
			if 41 <= los <= 45: return 'Heraldyka'
			if 46 <= los <= 50: return 'Szczescie'
			if 51 <= los <= 55: return 'Czytanie/Pisanie'
			if 55 <= los <= 60: return 'Wspinaczka'
			if 61 <= los <= 65: return 'Cichy chod w miescie'
			if 66 <= los <= 70: return 'Szosty zmysl'
			if 71 <= los <= 75: return 'Geniusz arytmetyczny'
			if 76 <= los <= 90: return 'Bardzo wytrzymaly'
			if 91 <= los <= 100: return 'Bardzo silny'
		if rasa == 'czlowiek':
			if 1 <= los <= 5: return 'Czuly sluch'
			if 6 <= los <= 10: return 'Oburecznosc'
			if 11 <= los <= 15: return 'Astronomia'
			if 16 <= los <= 20: return 'Gadanina'
			if 21 <= los <= 25: return 'Kryptografia'
			if 26 <= los <= 30: return 'Taniec'
			if 31 <= los <= 35: return 'Powozenie'
			if 36 <= los <= 40: return 'Bystry wzrok'
			if 41 <= los <= 45: return 'Ucieczka'
			if 46 <= los <= 50: return 'Heraldyka'
			if 51 <= los <= 55: return 'Szybki refleks'
			if 56 <= los <= 60: return 'Szczescie'
			if 61 <= los <= 65: return 'Czytanie/Pisanie'
			if 66 <= los <= 70: return 'Cichy chod na wsi'
			if 71 <= los <= 75: return 'Cichy chod w miescie'
			if 76 <= los <= 80: return 'Spiew'
			if 81 <= los <= 85: return 'Szosty zmysl'
			if 86 <= los <= 90: return 'Geniusz arytmetyczny'
			if 91 <= los <= 95: return 'Bardzo wytrzymaly'
			if 96 <= los <= 100: return 'Bardzo silny'

def getBasicSkills(rasa, wiek, klasa):
	if rasa == 'czlowiek':
		if 16 <= wiek <= 20 : mod = 0 
		elif 21 <= wiek <= 30 : mod = 1
		elif 31 <= wiek <= 40 : mod = 2
		elif 41 <= wiek <= 50 : mod = 1
		elif 51 <= wiek <= 60 : mod = 0
		elif 61 <= wiek <= 70 : mod = -1
		elif 71 <= wiek: mod = -2
	elif rasa == 'elf':
		if 16 <= wiek <= 40: mod = 0	
		elif 41 <= wiek <= 90: mod = 1
		elif 91 <= wiek <= 140: mod = 2
		elif 141 <= wiek <= 190: mod = 3
		elif 191 <= wiek <= 200: mod = 2
		elif 201 <= wiek: mod = 1
	elif rasa == 'krasnolud':
		if 16 <= wiek <= 40: mod = 0	
		elif 41 <= wiek <= 70: mod = 1
		elif 71 <= wiek <= 100: mod = 2
		elif 101 <= wiek <= 130: mod = 1
		elif 131 <= wiek <= 170: mod = 0
		elif 171 <= wiek <= 190: mod = -1
		elif 191 <= wiek: mod = -2
	elif rasa == 'halfling':
		if 16 <= wiek <= 30: mod = 0	
		elif 31 <= wiek <= 70: mod = 1
		elif 71 <= wiek <= 100: mod = 0
		elif 101 <= wiek <= 120: mod = -1
		elif 121 <= wiek: mod = -2
	numberOfBasicSkillsLeft = max(k(1,4) + mod, 0)
	skills = []
	while numberOfBasicSkillsLeft != 0:
		if rasa == 'czlowiek':
			skill = getUmiejetnoscZTabeli(rasa,klasa)
			if skill not in skills:
				skills.append(skill)
				numberOfBasicSkillsLeft -= 1
		if rasa == 'elf':
			if len(skills) == 0:
				skills.append('Bystry wzrok')
				numberOfBasicSkillsLeft -= 1
			elif len(skills) == 1:
				skills.append(choice(['Muzykalnosc', 'Taniec', 'Spiew']))
				numberOfBasicSkillsLeft -= 1
			else:
				skill = getUmiejetnoscZTabeli(rasa,klasa)
				if skill not in skills:
					skills.append(skill)
					numberOfBasicSkillsLeft -= 1
		if rasa == 'krasnolud':
			if len(skills) == 0:
				skills.append('Gornictwo')
				numberOfBasicSkillsLeft -= 1
			elif len(skills) == 1:
				skills.append(choice(['Kowalstwo', 'Metalurgia']))
				numberOfBasicSkillsLeft -= 1
			else:
				skill = getUmiejetnoscZTabeli(rasa,klasa)
				if skill not in skills:
					skills.append(skill)
					numberOfBasicSkillsLeft -= 1
		if rasa == 'halfling':
			if len(skills) == 0:
				skills.append('Gotowanie')
				numberOfBasicSkillsLeft -= 1
			elif len(skills) == 1:
				skills.append(choice(['Bron specjalna - proca', 'Zielarstwo', 'Cichy chod na wsi']))
				numberOfBasicSkillsLeft -= 1
			else:
				skill = getUmiejetnoscZTabeli(rasa,klasa)
				if skill not in skills:
					skills.append(skill)
					numberOfBasicSkillsLeft -= 1
	return skills
		
class classNpc:
	def __init__(self, rasa=None):
		self.imie = u"Nieznany"
		if rasa:
			if rasa in rasy: self.rasa=rasa
			else: raise "nienznana rasa!"
		else: self.rasa = choice(rasy)
		self.plec = choice([u'M', u'K'])
		self.pp = getPP(self.rasa)
		self.wiek = getWiek(self.rasa)
		while not hasattr(self, 'klasa') or not self.klasa in ['wojownik', 'ranger', 'lotr', 'uczony']:
			self.stats = getBasicStats(self.rasa)
			self.klasa = getKlasa(self.stats, self.rasa)
		self.skills = getBasicSkills(self.rasa, self.wiek, self.klasa)

	def __str__(self):
		w = u''
		w += u' ________________________________________________________________\n'
		w += u'|                         |           |   |      |          |    |\n'
		w += u'|           Imie          |   rasa    | P | wiek |  klasa   | PP |\n'
		w += u'|_________________________|___________|___|______|__________|____|\n' 
		w += u'|                         |           |   |      |          |    |\n'
		w += u"| {0:24}| {1:10}| {2} | {3:<4} | {4:<8} | {5:<2} |\n".format(self.imie, self.rasa, self.plec, self.wiek, self.klasa, self.pp)
		w += u'|_________________________|___________|___|______|__________|____|\n' 
		w += u' _____________________________________________________________________ \n'
		w += u'|    |    |    |    |    |    |    |    |    |    |    |    |    |    |\n'
		w += u'| Sz | WW | US | S  | Wt | Zw | I  | A  | Zr | CP | Int| Op | SW | Ogd|\n'
		w += u'|____|____|____|____|____|____|____|____|____|____|____|____|____|____|\n'
		w += u'|    |    |    |    |    |    |    |    |    |    |    |    |    |    |\n'
		w += u'| {0:<2} | {1} | {2} | {3:<2} | {4:<2} | {5:<2} | {6:<2} | {7:<2} | {8} | {9} | {10} | {11} | {12} | {13} |\n'.format(self.stats['Sz'], self.stats['WW'], self.stats['US'], self.stats['S'], self.stats['Wt'], self.stats['Zw'], self.stats['I'], self.stats['A'], self.stats['Zr'], self.stats['CP'], self.stats['Int'], self.stats['Op'], self.stats['SW'], self.stats['Ogd'])
		w += u'|____|____|____|____|____|____|____|____|____|____|____|____|____|____|\n'
		w += u' ________________________________\n'
		w += u'|                                |\n'
		w += u'|         umiejetnosci           |\n'
		w += u'|________________________________|\n' 	
		w += u'|                                |\n'
		for i in self.skills:
			w += u'| {:30} |\n'.format(i)
		w += u'|________________________________|\n'
		return w

def main():
	for i in xrange(10):
		print classNpc()
	k(10,20)	

if __name__ == '__main__':
	main()
	

"""	
	if rasa == "czlowiek": 
	elif rasa == "elf": 
	elif rasa == "krasnolud": 
	elif rasa == "halfling": 
"""
